// ==UserScript==
// @name         Talibri Nice Bonusses
// @namespace    http://tampermonkey.net/
// @version      2.1
// @description  Cleaning up the bonusses on Talibri.
// @author       Sankyuu
// @match        *://*talibri.com/*
// @require      https://gist.github.com/raw/2625891/waitForKeyElements.js
// @grant        GM_addStyle
// ==/UserScript==

function load() {
    'use strict';


    function restyle(){
        var targetElement = document.getElementById("bonus_details").children[0];
        targetElement.parentElement.style.width = 400 + "px";
        GM_addStyle('.bonus-enabled { background-color:green!IMPORTANT; }');
        GM_addStyle('.Fancyrow {margin-left:15px; margin-right:15px; width:100%;}');
        for(var i = 0; targetElement.childElementCount > i; i++){
            targetElement.children[i].classList = "Fancyrow";
            targetElement.children[i].children[0].children[0].width = 16;
            targetElement.children[i].children[0].children[0].height = 16;
            targetElement.children[i].children[0].style.width = 360 + "px";
            var data = targetElement.children[i].children[0].innerHTML.split(">");
            data[0] += ">";
            if(targetElement.children[i].children[0].getAttribute("data-original-title") !== null){
                data[1] = targetElement.children[i].children[0].getAttribute("data-original-title");
            } else{
                data[1] = targetElement.children[i].children[0].getAttribute("title");
            }
            data[1] = data[1].replace("Housing","");
            data[1] = data[1].replace(" Bonus","");
            data[1] = data[1].replace("Actions Remaining","Ticks");
            targetElement.children[i].children[0].innerHTML = data[0] + data[1];
        }
        //console.log("Updated bonusses!");
    }
    restyle();

    if(window.name !== "intervalSet"){
        var interval = setInterval(restyle, 6000);
        window.name = "intervalSet";
    }
};
window.onbeforeunload= function(e){window.name =""};
waitForKeyElements ("#bonus_details", load);